#!/bin/bash
LOG_DIR="/var/log/container"
RUNTIME_FOLDER="/runtime"
ENVIRONMENT="$1"

run_runtime_script() {
    if [ "$1" == "init" ] ; then
        local RUNTIME_FOLDER="/runtime/init"
    fi
    if [ -d $RUNTIME_FOLDER ] ; then
        for runtime_script in `find $RUNTIME_FOLDER -maxdepth 1 -type f -name "*.sh" -o -name "*.py" | sort` ; do
            local FNAME=`basename $runtime_script`
            if [ ".sh" == `echo $FNAME | tail -c 4` ] ; then
                echo "$(date +'[%d/%b/%Y %T]') Start $FNAME script" >> $LOG_DIR/runtime_$FNAME.log
                bash $runtime_script $CONF_FILE 1>>$LOG_DIR/runtime_$FNAME.log 2>>$LOG_DIR/runtime_$FNAME.err.log &
            elif [ ".py" == `echo $FNAME | tail -c 4` ] ; then
                echo "$(date +'[%d/%b/%Y %T]') Start $FNAME script" >> $LOG_DIR/runtime_$FNAME.log
                python $runtime_script $CONF_FILE 1>>$LOG_DIR/runtime_$FNAME.log 2>>$LOG_DIR/runtime_$FNAME.err.log &
            else
                echo "$(date +'[%d/%b/%Y %T]') Start $FNAME script" >> $LOG_DIR/runtime_err.log
                echo "RUNTIME FAILED! ($FNAME)" >> runtime_err.log
            fi
            sleep 1
        done
    fi
}

main() {
    if [ -f /initialized ] && [ `cat /initialized` == "1" ] ; then
        echo "$(date +'[%d/%b/%Y %T]') Start container" >> $LOG_DIR/container.log
    else
        echo "$(date +'[%d/%b/%Y %T]') Create container" >> $LOG_DIR/container.log
        local passwd=`cat /manage_passwd`
        local username="cepave"
        local cryptpasswd=$(perl -e 'print crypt($ARGV[0], "password")' $passwd)
        echo "$(date +'[%d/%b/%Y %T]') Add user cepave" >> $LOG_DIR/container.log
        useradd -m -s /bin/bash -p $cryptpasswd $username >> $LOG_DIR/container.log
        groupmod -g 999 $username
        usermod -aG sudo $username >> $LOG_DIR/container.log
        run_runtime_script init
        echo "1" > /initialized
    fi

    if [ "$ENVIRONMENT" == "-developer" -o "$ENVIRONMENT" == "-d" ] ; then
        service ssh start
        run_runtime_script
        exec /sbin/init
    fi
}


main "$@"
