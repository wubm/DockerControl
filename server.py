#!/usr/bin/python

import os, sys
import time
import socket
import re
import commands
import random, string

from src.message import * 

HOST, PORT = '', 8888
PATH = os.path.dirname(os.path.abspath(__file__))

listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
print('Serving HTTP on port %s ...' % PORT)

def randomword(size):
    return ''.join(random.choice(string.ascii_letters) for x in range(size))

def reset_container(container):
    cmd_stats, cmd_result = commands.getstatusoutput("docker rm -f %s" % container)
    if cmd_stats == 0:
        newpasswd = randomword(6)
        cmd_stats, cmd_result = commands.getstatusoutput("echo %s > %s/passwd/%s" % (newpasswd, PATH, container))
        if cmd_stats != 0:
            #client_connection.sendall("Reset failed in generate new password step, call Admin\nError message: %s" % cmd_result)
            client_connection.sendall(message("Reset failed in generate new password step, call Admin\nError message: ", cmd_result))
            return 1
        cmd_stats, cmd_result = commands.getstatusoutput("docker-compose -f %s/docker-compose.yml up -d %s" % (PATH, container))
        if cmd_stats == 0:
            #client_connection.sendall("Reset container done. New passwd is \"%s\"\n%s" % (newpasswd, suggest_message(container, newpasswd)))
            client_connection.sendall(message("Reset container done. New passwd is \"", newpasswd, "\"\n", suggest_message(container, newpasswd)))
            return 0
        else:
            #client_connection.sendall("Reset failed in create new container step, call Admin\nError message: %s" % cmd_result)
            client_connection.sendall(message("Reset failed in create new container step, call Admin\nError message: ", cmd_result))
            return 1
    else:
        #client_connection.sendall("Reset failed in rm container step, call Admin\nError message: %s" % cmd_result)
        client_connection.sendall(message("Reset failed in rm container step, call Admin\nError message: ", cmd_result))
        return 1

def restart_container(container, passwd):
    cmd_stats, cmd_result = commands.getstatusoutput("docker restart %s" % container)
    if cmd_stats == 0:
        #client_connection.sendall("Restart done.\n%s" % suggest_message(container, passwd))
        client_connection.sendall(message("Restart done.\n", suggest_message(container, passwd)))
        return 0
    else:
        #client_connection.sendall("Restart failed, call Admin\nError message: %s" % cmd_result)
        client_connection.sendall(message("Restart failed, call Admin\nError message: ", cmd_result))
        return 1

def start_container(container):
    cmd_stats, cmd_result = commands.getstatusoutput("docker start %s" % container)
    if cmd_stats == 0:
        #client_connection.sendall("HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n\r\n Start done.\n%s" % suggest_message(container))
        #client_connection.sendall("HTTP/1.0 200 OK\n\nStart done.\n%s" % suggest_message(container))
        client_connection.sendall(message("Start done.\n", suggest_message(container)))
        return 0
    else:
        cmd_stats, cmd_result = commands.getstatusoutput("docker-compose -f %s/docker-compose.yml up -d %s" % (PATH, container))
        if cmd_stats == 0:
            cmd_stats, passwd = commands.getstatusoutput("cat %s/passwd/%s" % (PATH, container))
            if cmd_stats == 0:
                #client_connection.sendall("Start container done. Your passwd is \"%s\"\n%s" % (passwd, suggest_message(container, passwd)))
                client_connection.sendall(message("Start container done. Your passwd is \"", passwd, "\"\n", suggest_message(container, passwd)))
            else:
                #client_connection.sendall("Start container done without passwd, please call Admin")
                client_connection.sendall(message("Start container done without passwd, please call Admin"))
            return 0
        else:
            #client_connection.sendall("Start failed, call Admin\nError message: %s" % cmd_result)
            client_connection.sendall(message("Start failed, call Admin\nError message: ", cmd_result))
            return 1

def stop_container(container, passwd):
    cmd_stats, cmd_result = commands.getstatusoutput("docker stop %s" % container)
    if cmd_stats == 0:
        #client_connection.sendall("Stop done.\n%s" % suggest_message(container, passwd))
        client_connection.sendall(message("Stop done.\n", suggest_message(container, passwd)))
        return 0
    else:
        #client_connection.sendall("Stop failed, call Admin\nError message: %s" % cmd_result)
        client_connection.sendall(message("Stop failed, call Admin\nError message: ", cmd_result))
        return 1

def rm_container(container):
    cmd_stats, cmd_result = commands.getstatusoutput("docker rm -f %s" % container)
    if cmd_stats == 0:
        newpasswd = randomword(6)
        cmd_stats, cmd_result = commands.getstatusoutput("echo %s > %s/passwd/%s" % (newpasswd, PATH, container))
        #client_connection.sendall("\n%s" % bye_message())
        client_connection.sendall(message(bye_message()))
        return 0
    else:
        #client_connection.sendall("Delete container failed, call Admin\nError message: %s" % cmd_result)
        client_connection.sendall(message("Delete container failed, call Admin\nError message: ", cmd_result))
        return 1

while True:
    try:
        client_connection, client_address = listen_socket.accept()
        request = client_connection.recv(1024).split(" ")
        request = request[1].split("?")
        func = re.search(r'\w+', request[0]).group(0)
        try:
            args = request[1].split("&")
        except:
            args = ""
        api_name = ""
        container_name = ""
        container_passwd = "" 
        print(func)
        print(args)

        support_func = [ "reset", "restart", "start", "stop", "brianisadog" , "rm"]
        for i in support_func:
            if i == func:
                api_name = func
        for i in args:
            keyvalue = i.split("=")
            if keyvalue[0] == "container" and keyvalue[1] != None:
                container_name = keyvalue[1]
                continue
            elif keyvalue[0] == "passwd" and keyvalue[1] != None:
                container_passwd = keyvalue[1]
                continue
        if api_name != "":
            if container_name != "":
                cmd_stats, cmd_result = commands.getstatusoutput("cat %s/passwd/%s" % (PATH, container_name))
                if cmd_stats != 0:
                    #client_connection.sendall("%s Error container_name" % error_message())
                    client_connection.sendall(message(error_message(), " Error container_name"))
                    client_connection.close()
                    continue
            else:
                if api_name != "brianisadog":
                    #client_connection.sendall("%s Need container arg" % error_message())
                    client_connection.sendall(message(error_message(), " Need container arg"))
                    client_connection.close()
                    continue
            if container_passwd == "":
                if api_name != "start" and api_name != "brianisadog":
                    #client_connection.sendall("%s Need passwd arg" % error_message())
                    client_connection.sendall(message(error_message(), " Need passwd arg"))
                    client_connection.close()
                    continue
            elif container_passwd != cmd_result:
                #client_connection.sendall("%s Error passwd" % error_message())
                client_connection.sendall(message(error_message(), " Error passwd"))
                client_connection.close()
                continue
            if api_name == "reset":
                reset_container(container_name)
            elif api_name == "restart":
                restart_container(container_name, container_passwd)
            elif api_name == "start":
                start_container(container_name)
            elif api_name == "stop":
                stop_container(container_name, container_passwd)
            elif api_name == "rm":
                rm_container(container_name)
            elif api_name == "brianisadog":
                #client_connection.sendall("%s"% BIRAN_MESSAGE)
                client_connection.sendall(message(BIRAN_MESSAGE))
        else:
            #client_connection.sendall("%s Error API" % error_message())
            client_connection.sendall(message(error_message(), " Error API"))
        client_connection.close()
    
    except IOError:
        print('An error occured trying to read the file.')
        
    except ValueError:
        print('Non-numeric data found in the file.')
    
    except ImportError:
        print("NO module found")
        
    except EOFError:
        print('Why did you do an EOF on me?')
    
    except KeyboardInterrupt:
        print('You cancelled the operation.')
        break
    except:
        #client_connection.sendall("%s ???? " % error_message())
        client_connection.sendall(message(error_message(), " ????"))
        client_connection.close()
        #raise
