#!/bin/bash
DIR_PATH=$(dirname $(readlink -f $0))
DC_FILE="$DIR_PATH/docker-compose.yml"
RUNTIME_FOLDER="/media/sf_Data/runtime"
DATA_ROOT_FOLDER="/media/sf_Data/data"
DATA_SHARE_FOLDER="/media/sf_Data/data/share"
LOG_FOLDER="/var/log/container"
PASSWD_FOLDER="$DIR_PATH/passwd"
CONTAINER_NUM=20
CONTAINER_PREFIX="ubuntu"
IMAGE_NAME="develop"
IMAGE_VERSION="1.0"

# Generate docker-compose.yml content
genereate_dc_content() {
    echo "version: \"3.1\"" > $DC_FILE
    echo "" >> $DC_FILE
    echo "services:" >> $DC_FILE
    for i in `seq $CONTAINER_NUM` ; do
        num=`printf "%02d" $i`
        port_init=10000
        port_range=10
        port_ssh=`expr $port_init + $i \* $port_range`
        port_start=`expr $port_init + 1 + $i \* $port_range`
        port_end=`expr $port_init + $port_range - 1 + $i \* $port_range`
        echo "  $CONTAINER_PREFIX$num:" >> $DC_FILE
        echo "    container_name: $CONTAINER_PREFIX$num" >> $DC_FILE
        echo "    image: $IMAGE_NAME:$IMAGE_VERSION" >> $DC_FILE
        echo "    hostname: $CONTAINER_PREFIX$num" >> $DC_FILE
        echo "    ports:" >> $DC_FILE
        echo "      - \"$port_ssh:22\"" >> $DC_FILE
        echo "      - \"$port_start-$port_end:$port_start-$port_end\"" >> $DC_FILE
        echo "    volumes:" >> $DC_FILE
        echo "      - /etc/localtime:/etc/localtime:ro" >> $DC_FILE
        echo "      - ./docker/docker-entrypoint.sh:/entrypoint.sh:ro" >> $DC_FILE
        echo "      - $RUNTIME_FOLDER/$CONTAINER_PREFIX$num:/runtime" >> $DC_FILE
        echo "      - $DATA_ROOT_FOLDER/$CONTAINER_PREFIX$num:/data/$CONTAINER_PREFIX$num" >> $DC_FILE
        echo "      - $DATA_SHARE_FOLDER:/data/share" >> $DC_FILE
        echo "      - $DATA_ROOT_FOLDER:/data:ro" >> $DC_FILE
        echo "      - $LOG_FOLDER/$CONTAINER_PREFIX$num:/var/log/container" >> $DC_FILE
        echo "      - $PASSWD_FOLDER/$CONTAINER_PREFIX$num:/manage_passwd:ro" >> $DC_FILE
        echo "    command: [ "-d" ]" >> $DC_FILE
    done
}

# Generate data folder
generate_data_folder() {
    for i in `seq $CONTAINER_NUM` ; do
        num=`printf "%02d" $i`
        if [ ! -d $DATA_ROOT_FOLDER/$CONTAINER_PREFIX$num ] ; then
            mkdir -p $DATA_ROOT_FOLDER/$CONTAINER_PREFIX$num
        fi
    done
}

# Generate runtime folder
generate_rt_folder() {
    for i in `seq $CONTAINER_NUM` ; do
        num=`printf "%02d" $i`
        if [ ! -d $RUNTIME_FOLDER/$CONTAINER_PREFIX$num ] ; then
            mkdir -p $RUNTIME_FOLDER/$CONTAINER_PREFIX$num
        fi
    done 
}

# Generate log folder
generate_lg_folder() {
    if [ ! -d $LOG_FOLDER ] ; then
        sudo mkdir -p $LOG_FOLDER
    fi
    sudo chown $(whoami) $LOG_FOLDER
    for i in `seq $CONTAINER_NUM` ; do
        num=`printf "%02d" $i`
        if [ ! -d $LOG_FOLDER/$CONTAINER_PREFIX$num ] ; then
            mkdir -p $LOG_FOLDER/$CONTAINER_PREFIX$num
        fi
    done

}

# Generate container passwd
generate_passwd() {
    if [ ! -f $PASSWD_FOLDER ] ; then
        mkdir -p $PASSWD_FOLDER
    fi
    for i in `seq $CONTAINER_NUM` ; do
        num=`printf "%02d" $i`
        if [ ! -f $PASSWD_FOLDER/$CONTAINER_PREFIX$num ] ; then
            string=`date +%s`."$CONTAINER_PREFIX$num"
            passwd=`echo $string | sha256sum | head -c 6 ; echo`
            echo "$passwd" > $PASSWD_FOLDER/$CONTAINER_PREFIX$num
        fi
    done
}

main() {
    if [ "$1" == "build" ] ; then
        cd $DIR_PATH/docker
        docker build -t $IMAGE_NAME:$IMAGE_VERSION .
    elif [ "$1" == "prepare" ] ; then
        generate_passwd
        generate_data_folder
        generate_lg_folder
        generate_rt_folder
        genereate_dc_content
    else
        echo "arg: build or prepare"
    fi
}

main "$@"
